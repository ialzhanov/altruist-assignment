package com.altruist.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Class represents a composed message data transfer object
 *
 * @author ialzhanov
 * @since 2/7/2020
 */
@Data
@ApiModel(value = "Message", description = "Class represents a composed message data transfer object")
public class MessageDto {

    @JsonProperty(value = "messageText")
    @ApiModelProperty(
            notes = "Message text",
            name = "text",
            required = true,
            value = "Sample message text"
    )
    private String text;
}
