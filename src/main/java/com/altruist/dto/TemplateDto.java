package com.altruist.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.altruist.AltruistAssignmentApp.*;

/**
 * Class represents a template data transfer object
 *
 * @author ialzhanov
 * @since 2/7/2020
 */
@Data
@ApiModel(value = "Template", description = "Class represents a template data transfer object")
public class TemplateDto {

    @JsonProperty(value = "templateId")
    @NotNull(message = TEMPLATE_DTO_ID_CANNOT_BE_NULL)
    @ApiModelProperty(
            notes = "Template identifier",
            name = "id",
            required = true,
            value = "template_identifier"
    )
    private String id;

    @JsonProperty(value = "templateText")
    @NotNull(message = TEMPLATE_DTO_TEXT_CANNOT_BE_NULL)
    @Size(
            min = 3,
            max = 128,
            message = TEMPLATE_DTO_TEXT_SIZE_BETWEEN
    )
    @ApiModelProperty(
            notes = "Template text",
            name = "text",
            required = true,
            value = "Sample message template, $hello_name"
    )
    private String text;
}
