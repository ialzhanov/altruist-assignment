package com.altruist.exception;

/**
 * Class represents exception thrown, in case another template with the same identifier already exists in the database
 *
 * @author ialzhanov
 * @see com.altruist.service.TemplateService
 * @since 2/7/2020
 */
public class TemplateWithSameIdentifierAlreadyExists extends RuntimeException {
    public TemplateWithSameIdentifierAlreadyExists(String message) {
        super(message);
    }
}
