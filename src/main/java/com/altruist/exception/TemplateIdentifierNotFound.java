package com.altruist.exception;

/**
 * Class represents exception thrown, in case no template can be found in the database by the given identifier
 *
 * @author ialzhanov
 * @see com.altruist.service.TemplateService
 * @since 2/7/2020
 */
public class TemplateIdentifierNotFound extends RuntimeException {
    public TemplateIdentifierNotFound(String message) {
        super(message);
    }
}
