package com.altruist.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.altruist.AltruistAssignmentApp.*;

/**
 * Class represents a template entity
 *
 * @author ialzhanov
 * @since 2/7/2020
 */
@Data
@Entity
@Table(name = "TEMPLATES")
public class Template {

    @Id
    @Column(name = "ID")
    @NotNull(message = TEMPLATE_ID_CANNOT_BE_NULL)
    private String id;

    @Column(name = "TEXT", nullable = false)
    @NotNull(message = TEMPLATE_TEXT_CANNOT_BE_NULL)
    @Size(
            min = 3,
            max = 128,
            message = TEMPLATE_TEXT_SIZE_BETWEEN
    )
    private String text;
}
