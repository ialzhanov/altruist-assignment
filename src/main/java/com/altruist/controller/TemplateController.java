package com.altruist.controller;

import com.altruist.dto.MessageDto;
import com.altruist.dto.TemplateDto;
import com.altruist.service.TemplateService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

import static com.altruist.AltruistAssignmentApp.*;
import static org.springframework.http.ResponseEntity.ok;

/**
 * @author ialzhanov
 * @since 2/7/2020
 */
@RestController
@RequestMapping(TEMPLATES_BASE_URL)
public class TemplateController implements TemplateAPI {

    private TemplateService templateService;

    public TemplateController(TemplateService templateService) {
        this.templateService = templateService;
    }

    @PostMapping
    @Override
    public ResponseEntity<String> submit(@Valid @RequestBody TemplateDto templateDto) {
        templateService.submit(templateDto);
        return ok(TEMPLATE_CREATED_SUCCESSFULLY);
    }

    @GetMapping
    @Override
    public ResponseEntity<Iterable<TemplateDto>> retrieve() {
        return ok(templateService.retrieve());
    }

    @GetMapping(TEMPLATES_COMPOSE_URL)
    @Override
    public ResponseEntity<MessageDto> compose(@PathVariable(value = "templateId") String templateId,
                                              @RequestParam Map<String, String> templateVariableMap) {
        return ok(templateService.compose(templateId, templateVariableMap));
    }
}
