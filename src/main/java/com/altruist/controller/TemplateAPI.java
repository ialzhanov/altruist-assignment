package com.altruist.controller;

import com.altruist.dto.MessageDto;
import com.altruist.dto.TemplateDto;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

/**
 * @author ialzhanov
 * @since 2/7/2020
 */
@Api(value = "TemplateAPI", protocols = "http, https")
public interface TemplateAPI {

    /**
     * Submit a new template
     * <p>
     * POST /templates
     * {
     * "templateId" : string,
     * "templateText" : string
     * }
     * <p>
     * response:
     * <p>
     * HTTP 200:
     * {
     * "Template is created successfully"
     * }
     * <p>
     * or
     * <p>
     * HTTP 400:
     * {
     * "Template id existed. Please choose another template id"
     * }
     * <p>
     * Note: template Text can contain multiple single-valued variables.
     * Single-valued variable is in the form of $variable_name.
     * <p>
     * Example:
     * <p>
     * POST /templates
     * {
     * "templateId" : "hello",
     * "templateText" : "Hello, $hello_name!"
     * }
     *
     * @return "Template is created successfully" or "Template id existed. Please choose another template id"
     */
    @ApiOperation(
            value = "Submit a template",
            httpMethod = "POST",
            response = String.class,
            consumes = APPLICATION_JSON_VALUE,
            produces = TEXT_PLAIN_VALUE
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            code = 200,
                            message = "Template is created successfully"
                    ),
                    @ApiResponse(
                            code = 400,
                            message = "Template id existed. Please choose another template id"
                    )
            }
    )
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "templateDto",
                    value = "Template object",
                    required = true,
                    dataType = "com.altruist.dto.TemplateDto",
                    paramType = "body"
            )
    })
    ResponseEntity<String> submit(TemplateDto templateDto);

    /**
     * Retrieve all templates
     * <p>
     * GET /templates
     * <p>
     * response:
     * <p>
     * HTTP 200:
     * [
     * {
     * "templateId" : string,
     * "templateText" : string
     * }
     * ]
     * <p>
     * Example:
     * <p>
     * GET /templates
     * <p>
     * response:
     * {
     * "templateId" : "hello",
     * "templateText" : "Hello, $hello_name!"
     * }
     *
     * @return list of templates
     */
    @ApiOperation(
            value = "Retrieve templates",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE,
            response = TemplateDto.class,
            responseContainer = "List"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            code = 200,
                            message = "Templates are retrieved successfully"
                    )
            }
    )
    ResponseEntity<Iterable<TemplateDto>> retrieve();

    /**
     * Compose a message based on the specified template
     * <p>
     * GET /templates/{templateId}/compose?key=value&....
     * <p>
     * HTTP 200:
     * {
     * "messageText" : string
     * }
     * <p>
     * or
     * <p>
     * HTTP 400:
     * {
     * "template id not found"
     * }
     * <p>
     * Note: when template variables are not provided, simply return the message with original text.
     * Parameter names are dynamically defined by caller.
     * <p>
     * Example:
     * <p>
     * GET /templates/hello/compose?hello_name=George
     * <p>
     * response:
     * {
     * "messageText" : "Hello, George!"
     * }
     * <p>
     * GET /templates/hello/compose?notexisting=foobar
     * <p>
     * response:
     * {
     * "messageText" : "Hello, $hello_name!"
     * }
     * <p>
     * GET /templates/notexisting/compose
     * <p>
     * response:
     * HTTP 400:
     * {
     * "template id not found"
     * }
     *
     * @return composed message
     */
    @ApiOperation(
            value = "Compose message",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE,
            response = MessageDto.class,
            notes = "Unfortunately, dynamic query parameters definition is not supported, so this call will not work from swagger. When calling API using any other clients, pass template variables as query parameters. Example: key1=value1&key2=value2"
    )
    @ApiResponses(value = {
            @ApiResponse(
                    code = 200,
                    message = "Template message is composed successfully"
            ),
            @ApiResponse(
                    code = 400,
                    message = "Template id not found"
            )
    })
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "templateId",
                    value = "Template identifier",
                    required = true,
                    dataType = "string",
                    paramType = "path",
                    example = "template_identifier"
            ),
            @ApiImplicitParam(
                    name = "templateVariableMap",
                    value = "Template variables",
                    paramType = "query",
                    example = "value&key1=value1&key2=value2&keyN=valueN"
            )
    })
    ResponseEntity<MessageDto> compose(String templateId, Map<String, String> templateVariableMap);
}
