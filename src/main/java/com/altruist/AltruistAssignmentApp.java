package com.altruist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ialzhanov
 * @since 2/7/2020
 */
@SpringBootApplication
public class AltruistAssignmentApp {

    public static final String TEMPLATES_BASE_URL = "/templates";
    public static final String TEMPLATES_COMPOSE_URL = "/{templateId}/compose";
    public static final String MIN_SIZE_VAR = "{min}";
    public static final String MAX_SIZE_VAR = "{max}";
    public static final Integer TEMPLATE_TEXT_MIN_LENGTH = 3;
    public static final Integer TEMPLATE_TEXT_MAX_LENGTH = 128;
    public static final String TEMPLATE_TEXT_TOO_SHORT = "a";
    public static final String TEMPLATE_TEXT_TOO_LONG = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
    public static final String TEMPLATE_ID_CANNOT_BE_NULL = "Template.id cannot be null";
    public static final String TEMPLATE_TEXT_CANNOT_BE_NULL = "Template.text cannot be null";
    public static final String TEMPLATE_TEXT_SIZE_BETWEEN = "Template.text size must be between "
            + MIN_SIZE_VAR + " and " + MAX_SIZE_VAR;
    public static final String TEMPLATE_TEXT_SIZE_BETWEEN_MESSAGE = TEMPLATE_TEXT_SIZE_BETWEEN
            .replace(MIN_SIZE_VAR, String.valueOf(TEMPLATE_TEXT_MIN_LENGTH))
            .replace(MAX_SIZE_VAR, String.valueOf(TEMPLATE_TEXT_MAX_LENGTH));
    public static final Integer TEMPLATE_DTO_TEXT_MIN_LENGTH = TEMPLATE_TEXT_MIN_LENGTH;
    public static final Integer TEMPLATE_DTO_TEXT_MAX_LENGTH = TEMPLATE_TEXT_MAX_LENGTH;
    public static final String TEMPLATE_DTO_TEXT_TOO_SHORT = TEMPLATE_TEXT_TOO_SHORT;
    public static final String TEMPLATE_DTO_TEXT_TOO_LONG = TEMPLATE_TEXT_TOO_LONG;
    public static final String TEMPLATE_DTO_ID_CANNOT_BE_NULL = "TemplateDto.id cannot be null";
    public static final String TEMPLATE_DTO_TEXT_CANNOT_BE_NULL = "TemplateDto.text cannot be null";
    public static final String TEMPLATE_DTO_TEXT_SIZE_BETWEEN = "TemplateDto.text size must be between "
            + MIN_SIZE_VAR + " and " + MAX_SIZE_VAR;
    public static final String TEMPLATE_DTO_TEXT_SIZE_BETWEEN_MESSAGE = TEMPLATE_DTO_TEXT_SIZE_BETWEEN
            .replace(MIN_SIZE_VAR, String.valueOf(TEMPLATE_DTO_TEXT_MIN_LENGTH))
            .replace(MAX_SIZE_VAR, String.valueOf(TEMPLATE_DTO_TEXT_MAX_LENGTH));
    public static final String TEMPLATE_CREATED_SUCCESSFULLY = "Template is created successfully";
    public static final String TEMPLATE_ID_EXISTED = "Template id existed. Please choose another template id";
    public static final String TEMPLATE_ID_NOT_FOUND = "Template id not found";
    public static final String TEMPLATE_SERVICE_PARSE_MESSAGE_CANNOT_BE_NULL = "TemplateService.parse: message must be provided";

    public static void main(String[] args) {
        SpringApplication.run(AltruistAssignmentApp.class, args);
    }
}
