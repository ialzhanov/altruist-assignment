package com.altruist.service;

import com.altruist.dto.MessageDto;
import com.altruist.dto.TemplateDto;
import com.altruist.exception.TemplateIdentifierNotFound;
import com.altruist.exception.TemplateWithSameIdentifierAlreadyExists;
import com.altruist.mapper.TemplateMapper;
import com.altruist.model.Template;
import com.altruist.repository.TemplateRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.altruist.AltruistAssignmentApp.*;

/**
 * @author ialzhanov
 * @since 2/7/2020
 */
public class TemplateService {

    /**
     * Special symbol that marks a template variable
     */
    private String delimiter;
    /**
     * Class handles dto-to-entity and entity-to-dto mappings
     */
    private TemplateMapper templateMapper;
    /**
     * Template repository
     */
    private TemplateRepository templateRepository;

    public TemplateService(String delimiter, TemplateMapper templateMapper, TemplateRepository templateRepository) {
        this.delimiter = delimiter;
        this.templateMapper = templateMapper;
        this.templateRepository = templateRepository;
    }

    /**
     * @param templateDto object to persist
     * @return persisted template object
     * @throws TemplateWithSameIdentifierAlreadyExists in case entity with the same identifier already exists
     */
    public Template submit(TemplateDto templateDto) {
        Template template = templateMapper.mapFromDtoToModel(templateDto);
        if (templateRepository.existsById(template.getId())) {
            throw new TemplateWithSameIdentifierAlreadyExists(TEMPLATE_ID_EXISTED);
        }
        return templateRepository.save(templateMapper.mapFromDtoToModel(templateDto));
    }

    /**
     * @return list of persisted templates
     */
    public Iterable<TemplateDto> retrieve() {
        List<TemplateDto> result = new LinkedList<>();
        Iterable<Template> templateList = templateRepository.findAll();
        templateList.forEach(template -> result.add(templateMapper.mapFromModelToDto(template)));
        return result;
    }

    /**
     * @param templateId          template identifier
     * @param templateVariableMap map of template variable values
     * @return composed template message
     * @throws TemplateIdentifierNotFound in case no template with such identifier exists in the database
     */
    public MessageDto compose(String templateId, Map<String, String> templateVariableMap) {
        Optional<Template> persisted = templateRepository.findById(templateId);
        if (persisted.isPresent()) {
            Template template = persisted.get();
            MessageDto messageDto = new MessageDto();
            messageDto.setText(parse(template.getText(), templateVariableMap));
            return messageDto;
        } else {
            throw new TemplateIdentifierNotFound(TEMPLATE_ID_NOT_FOUND);
        }
    }

    /**
     * If no variables are passed, then original message is returned
     *
     * @param message     text to parse
     * @param variableMap map of template variable values
     * @return parsed template message
     * @throws IllegalArgumentException if message is null
     */
    protected String parse(String message, Map<String, String> variableMap) {
        if (message == null || message.isEmpty()) {
            throw new IllegalArgumentException(TEMPLATE_SERVICE_PARSE_MESSAGE_CANNOT_BE_NULL);
        }
        if (variableMap == null || variableMap.isEmpty()) {
            return message;
        }
        StringBuilder tempKey;
        String key;
        String entryKey;
        for (Map.Entry<String, String> entry : variableMap.entrySet()) {
            entryKey = entry.getKey();
            tempKey = new StringBuilder();
            if (entryKey.startsWith(delimiter)) {
                tempKey.append(entryKey);
            } else {
                tempKey.append(delimiter).append(entryKey);
            }
            key = tempKey.toString();
            if (message.contains(key)) {
                message = message.replace(key, entry.getValue());
            }
        }
        return message;
    }
}
