package com.altruist.config;

import com.altruist.mapper.TemplateMapper;
import com.altruist.repository.TemplateRepository;
import com.altruist.service.TemplateService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ialzhanov
 * @since 2/7/2020
 */
@Configuration
public class TemplateServiceConfig {

    @Bean
    public TemplateService templateService(@Value("${templateService.delimiter}") String delimiter,
                                           TemplateMapper templateMapper,
                                           TemplateRepository templateRepository) {
        return new TemplateService(delimiter, templateMapper, templateRepository);
    }
}
