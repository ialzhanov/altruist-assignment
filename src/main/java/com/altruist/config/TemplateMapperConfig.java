package com.altruist.config;

import com.altruist.mapper.TemplateMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ialzhanov
 * @since 2/7/2020
 */
@Configuration
public class TemplateMapperConfig {

    @Bean
    public TemplateMapper templateMapper() {
        return Mappers.getMapper(TemplateMapper.class);
    }
}
