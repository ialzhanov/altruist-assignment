package com.altruist.advice;

import com.altruist.exception.TemplateIdentifierNotFound;
import com.altruist.exception.TemplateWithSameIdentifierAlreadyExists;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * Centralized exception handling
 *
 * @author ialzhanov
 * @since 2/7/2020
 */
@ControllerAdvice
public class ResponseEntityExceptionHandlerAdvice extends ResponseEntityExceptionHandler {

    /**
     * Handles {@link com.altruist.exception.TemplateIdentifierNotFound} exception thrown when no template is
     * found by the provided identifier, and {@link com.altruist.exception.TemplateWithSameIdentifierAlreadyExists}
     * thrown when entity with the provided identifier already exists
     *
     * @param ex      the exception
     * @param request the current request
     * @return response with status code 400 and exception message as a body
     */
    @ExceptionHandler(value = {
            TemplateIdentifierNotFound.class,
            TemplateWithSameIdentifierAlreadyExists.class,
    })
    public ResponseEntity<Object> handle(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(
                ex,
                ex.getMessage(),
                new HttpHeaders(),
                BAD_REQUEST,
                request
        );
    }

    /**
     * Handles {@link org.springframework.web.bind.MethodArgumentNotValidException} exception thrown when data transfer
     * object fails to pass validation checks
     *
     * @param ex      the exception
     * @param headers the headers to be written to the response
     * @param status  the selected response status
     * @param request the current request
     * @return response with status code 400 and validation errors concatenated into a single string as a body
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        StringBuilder sb = new StringBuilder();
        ex.getBindingResult().getAllErrors().forEach(e -> sb.append(e.getDefaultMessage()).append("; "));
        return handleExceptionInternal(
                ex,
                sb.toString(),
                headers,
                BAD_REQUEST,
                request
        );
    }
}
