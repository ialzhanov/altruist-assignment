package com.altruist.repository;

import com.altruist.model.Template;
import org.springframework.data.repository.CrudRepository;

/**
 * @author ialzhanov
 * @since 2/7/2020
 */
public interface TemplateRepository extends CrudRepository<Template, String> {
}
