package com.altruist.mapper;

import com.altruist.dto.TemplateDto;
import com.altruist.model.Template;
import org.mapstruct.Mapper;

/**
 * Class maps {@link com.altruist.dto.TemplateDto} into {@link com.altruist.model.Template}
 *
 * @author ialzhanov
 * @since 2/7/2020
 */
@Mapper(componentModel = "spring")
public interface TemplateMapper {

    /**
     * @param template object to map
     * @return {@link com.altruist.dto.TemplateDto} entity
     */
    TemplateDto mapFromModelToDto(Template template);

    /**
     * @param templateDto object to map
     * @return {@link com.altruist.model.Template} entity
     */
    Template mapFromDtoToModel(TemplateDto templateDto);
}
