package com.altruist.mapper;

import com.altruist.dto.TemplateDto;
import com.altruist.model.Template;
import org.junit.Test;
import org.mapstruct.factory.Mappers;

import static org.junit.Assert.assertEquals;

/**
 * Class tests template model to DTO conversion and vice versa
 *
 * @author ialzhanov
 * @since 2/7/2020
 */
public class TemplateMapperTest {

    private TemplateMapper templateMapper = Mappers.getMapper(TemplateMapper.class);

    /**
     * Test DTO to model conversion
     */
    @Test
    public void testDtoToModelConversion() {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("template_id");
        templateDto.setText("template text");
        Template template = templateMapper.mapFromDtoToModel(templateDto);
        assertEquals(template.getId(), templateDto.getId());
        assertEquals(template.getText(), templateDto.getText());
    }

    /**
     * Test DTO to model conversion with NULL values
     */
    @Test
    public void testNullDtoToModelConversion() {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId(null);
        templateDto.setText(null);
        Template template = templateMapper.mapFromDtoToModel(templateDto);
        assertEquals(template.getId(), templateDto.getId());
        assertEquals(template.getText(), templateDto.getText());
    }

    /**
     * Test model to DTO conversion
     */
    @Test
    public void testModelToDtoConversion() {
        Template template = new Template();
        template.setId("template_id");
        template.setText("template text");
        TemplateDto templateDto = templateMapper.mapFromModelToDto(template);
        assertEquals(templateDto.getId(), template.getId());
        assertEquals(templateDto.getText(), template.getText());
    }

    /**
     * Test model to DTO conversion with NULL values
     */
    @Test
    public void testNullModelToDtoConversion() {
        Template template = new Template();
        template.setId(null);
        template.setText(null);
        TemplateDto templateDto = templateMapper.mapFromModelToDto(template);
        assertEquals(templateDto.getId(), template.getId());
        assertEquals(templateDto.getText(), template.getText());
    }
}
