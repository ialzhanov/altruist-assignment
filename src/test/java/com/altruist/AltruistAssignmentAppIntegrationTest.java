package com.altruist;

import com.altruist.dto.MessageDto;
import com.altruist.dto.TemplateDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static com.altruist.AltruistAssignmentApp.*;
import static org.junit.Assert.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

/**
 * @author ialzhanov
 * @since 2/7/2020
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class AltruistAssignmentAppIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Submits template
     */
    @Test
    public void testSubmit200() {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_12");
        templateDto.setText("Hi, $user1!");
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
    }

    /**
     * Submits template with no identifier
     */
    @Test
    public void testSubmit400MissingIdentifier() {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setText("Hi, $user1!");
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().contains(TEMPLATE_DTO_ID_CANNOT_BE_NULL));
    }

    /**
     * Submits template with existing identifier
     */
    @Test
    public void testSubmit400IdentifierExists() {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_31");
        templateDto.setText("Hi, $user1!");
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
        response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(BAD_REQUEST, response.getStatusCode());
        assertEquals(TEMPLATE_ID_EXISTED, response.getBody());
    }

    /**
     * Submits template with no text
     */
    @Test
    public void testSubmit400MissingText() {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_34");
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().contains(TEMPLATE_DTO_TEXT_CANNOT_BE_NULL));
    }

    /**
     * Submits template with text being too short
     */
    @Test
    public void testSubmit400TextTooShort() {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_37");
        templateDto.setText(TEMPLATE_DTO_TEXT_TOO_SHORT);
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().contains(TEMPLATE_DTO_TEXT_SIZE_BETWEEN_MESSAGE));
    }

    /**
     * Submits template with text being too long
     */
    @Test
    public void testSubmit400TextTooLong() {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_3");
        templateDto.setText(TEMPLATE_DTO_TEXT_TOO_LONG);
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertTrue(response.getBody().contains(TEMPLATE_DTO_TEXT_SIZE_BETWEEN_MESSAGE));
    }

    /**
     * First saves three templates then retrieves them
     */
    @Test
    public void testRetrieve() {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_1834");
        templateDto.setText("Hi, $user1!");
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
        templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_18342");
        templateDto.setText("Hi, $user2!");
        response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
        templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_1834443");
        templateDto.setText("Hi, $user3!");
        response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
        templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_18344");
        templateDto.setText("Hi, $user4!");
        response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
        ResponseEntity<TemplateDto[]> responseList = restTemplate.getForEntity(TEMPLATES_BASE_URL, TemplateDto[].class);
        assertEquals(OK, responseList.getStatusCode());
        assertNotNull(responseList.getBody());
        List<TemplateDto> list = Arrays.asList(responseList.getBody());
        assertTrue(list.size() >= 4);
    }

    /**
     * Composes a message with all variables provided via query parameters
     */
    @Test
    public void testCompose200() throws JsonProcessingException {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_16666834");
        templateDto.setText("Dear $title$user");
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
        response = restTemplate.getForEntity(
                TEMPLATES_BASE_URL + "/a_new_template_id_16666834/compose?title=Mr.&user=User", String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        MessageDto messageDto = new MessageDto();
        messageDto.setText("Dear Mr.User");
        assertEquals(new ObjectMapper().writeValueAsString(messageDto), response.getBody());
    }

    /**
     * Composes a message with no variables provided and none were expected
     */
    @Test
    public void testCompose200NoneProvidedNoneExpected() throws JsonProcessingException {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_166776834");
        templateDto.setText("Dear user");
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
        response = restTemplate.getForEntity(
                TEMPLATES_BASE_URL + "/a_new_template_id_166776834/compose", String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        MessageDto messageDto = new MessageDto();
        messageDto.setText("Dear user");
        assertEquals(new ObjectMapper().writeValueAsString(messageDto), response.getBody());
    }

    /**
     * Composes a message with some template variables missing
     */
    @Test
    public void testCompose200NotAllVariablesProvided() throws JsonProcessingException {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_1698264834");
        templateDto.setText("Dear $title$user");
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
        response = restTemplate.getForEntity(
                TEMPLATES_BASE_URL + "/a_new_template_id_1698264834/compose?user=User", String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        MessageDto messageDto = new MessageDto();
        messageDto.setText("Dear $titleUser");
        assertEquals(new ObjectMapper().writeValueAsString(messageDto), response.getBody());
    }

    /**
     * Composes a message when template expects variables but none were provided
     */
    @Test
    public void testCompose200NoTemplateVariablesProvidedButExpected() throws JsonProcessingException {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_3335564834");
        templateDto.setText("Dear $title$user");
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
        response = restTemplate.getForEntity(
                TEMPLATES_BASE_URL + "/a_new_template_id_3335564834/compose", String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        MessageDto messageDto = new MessageDto();
        messageDto.setText("Dear $title$user");
        assertEquals(new ObjectMapper().writeValueAsString(messageDto), response.getBody());
    }

    /**
     * Composes a message with template does not expect variables and none were provided
     */
    @Test
    public void testCompose200NoTemplateVariablesExpectedButProvided() throws JsonProcessingException {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_164526666834");
        templateDto.setText("Dear user");
        ResponseEntity<String> response = restTemplate.postForEntity(TEMPLATES_BASE_URL, templateDto, String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        assertEquals(TEMPLATE_CREATED_SUCCESSFULLY, response.getBody());
        response = restTemplate.getForEntity(
                TEMPLATES_BASE_URL + "/a_new_template_id_164526666834/compose?title=Mr.&user=User", String.class);
        assertNotNull(response);
        assertEquals(OK, response.getStatusCode());
        MessageDto messageDto = new MessageDto();
        messageDto.setText("Dear user");
        assertEquals(new ObjectMapper().writeValueAsString(messageDto), response.getBody());
    }

    /**
     * Composes a message for a using non-existent identifier
     */
    @Test
    public void testCompose400NotFound() {
        ResponseEntity<String> response = restTemplate.getForEntity(
                TEMPLATES_BASE_URL + "/a_new_template_id_161134/compose?title=Mr.", String.class);
        assertNotNull(response);
        assertEquals(BAD_REQUEST, response.getStatusCode());
        assertEquals(TEMPLATE_ID_NOT_FOUND, response.getBody());
    }
}
