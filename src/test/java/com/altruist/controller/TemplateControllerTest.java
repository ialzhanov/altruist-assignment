package com.altruist.controller;

import com.altruist.dto.MessageDto;
import com.altruist.dto.TemplateDto;
import com.altruist.exception.TemplateIdentifierNotFound;
import com.altruist.exception.TemplateWithSameIdentifierAlreadyExists;
import com.altruist.service.TemplateService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static com.altruist.AltruistAssignmentApp.*;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test template controller error handling and data validation
 *
 * @author ialzhanov
 * @since 2/7/2020
 */
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {TemplateController.class})
public class TemplateControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TemplateService templateService;

    @Test
    public void testSubmit200() throws Exception {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id");
        templateDto.setText("Hi, $user!");
        mvc.perform(post(TEMPLATES_BASE_URL)
                .content(new ObjectMapper().writeValueAsString(templateDto))
                .contentType(APPLICATION_JSON)
                .accept(TEXT_PLAIN))
                .andExpect(status().is(OK.value()))
                .andExpect(content().string(TEMPLATE_CREATED_SUCCESSFULLY));
    }

    @Test
    public void testSubmit400MissingIdentifier() throws Exception {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setText("Hey, you!");
        mvc.perform(post(TEMPLATES_BASE_URL)
                .content(new ObjectMapper().writeValueAsString(templateDto))
                .contentType(APPLICATION_JSON)
                .accept(TEXT_PLAIN))
                .andExpect(status().is(BAD_REQUEST.value()))
                .andExpect(content().string(containsString(TEMPLATE_DTO_ID_CANNOT_BE_NULL)));
    }

    @Test
    public void testSubmit400MissingText() throws Exception {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("sample_template_identifier");
        mvc.perform(post(TEMPLATES_BASE_URL)
                .content(new ObjectMapper().writeValueAsString(templateDto))
                .contentType(APPLICATION_JSON)
                .accept(TEXT_PLAIN))
                .andExpect(status().is(BAD_REQUEST.value()))
                .andExpect(content().string(containsString(TEMPLATE_DTO_TEXT_CANNOT_BE_NULL)));
    }

    @Test
    public void testSubmit400TextTooShort() throws Exception {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("sample_template_identifier");
        templateDto.setText(TEMPLATE_DTO_TEXT_TOO_SHORT);
        mvc.perform(post(TEMPLATES_BASE_URL)
                .content(new ObjectMapper().writeValueAsString(templateDto))
                .contentType(APPLICATION_JSON)
                .accept(TEXT_PLAIN))
                .andExpect(status().is(BAD_REQUEST.value()))
                .andExpect(content().string(containsString(TEMPLATE_DTO_TEXT_SIZE_BETWEEN_MESSAGE)));
    }

    @Test
    public void testSubmit400TextTooLong() throws Exception {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("sample_template_identifier");
        templateDto.setText(TEMPLATE_DTO_TEXT_TOO_LONG);
        mvc.perform(post(TEMPLATES_BASE_URL)
                .content(new ObjectMapper().writeValueAsString(templateDto))
                .contentType(APPLICATION_JSON)
                .accept(TEXT_PLAIN))
                .andExpect(status().is(BAD_REQUEST.value()))
                .andExpect(content().string(containsString(TEMPLATE_DTO_TEXT_SIZE_BETWEEN_MESSAGE)));
    }

    @Test
    public void testSubmit400IdentifierExists() throws Exception {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id");
        templateDto.setText("Hi, $user!");
        when(templateService.submit(templateDto)).thenThrow(
                new TemplateWithSameIdentifierAlreadyExists(TEMPLATE_ID_EXISTED));
        mvc.perform(post(TEMPLATES_BASE_URL)
                .content(new ObjectMapper().writeValueAsString(templateDto))
                .contentType(APPLICATION_JSON)
                .accept(TEXT_PLAIN))
                .andExpect(status().is(BAD_REQUEST.value()))
                .andExpect(content().string(TEMPLATE_ID_EXISTED));
    }

    @Test
    public void testRetrieveWhenNoneExists() throws Exception {
        when(templateService.retrieve()).thenReturn(Collections.emptyList());
        mvc.perform(get(TEMPLATES_BASE_URL)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().is(OK.value()))
                .andExpect(content().string(new ObjectMapper().writeValueAsString(new ArrayList<>())));
    }

    @Test
    public void testRetrieve() throws Exception {
        List<TemplateDto> list = new LinkedList<>();
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_1");
        templateDto.setText("Hi, $user1!");
        list.add(templateDto);
        templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_2");
        templateDto.setText("Hi, $user2!");
        list.add(templateDto);
        templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_3");
        templateDto.setText("Hi, $user3!");
        list.add(templateDto);
        templateDto = new TemplateDto();
        templateDto.setId("a_new_template_id_4");
        templateDto.setText("Hi, $user4!");
        list.add(templateDto);
        when(templateService.retrieve()).thenReturn(list);
        mvc.perform(get(TEMPLATES_BASE_URL)
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().is(OK.value()))
                .andExpect(content().string(new ObjectMapper().writeValueAsString(list)));
    }

    @Test
    public void testCompose200() throws Exception {
        MessageDto messageDto = new MessageDto();
        messageDto.setText("Hey, value1 or value2!");
        String templateId = "template_identifier";
        Map<String, String> map = new HashMap<>();
        map.put("key1", "value1");
        map.put("key2", "value2");
        when(templateService.compose(templateId, map)).thenReturn(messageDto);
        mvc.perform(get(TEMPLATES_BASE_URL + "/" + templateId + "/compose?key1=value1&key2=value2")
                .accept(APPLICATION_JSON_VALUE))
                .andExpect(status().is(OK.value()))
                .andExpect(content().string(new ObjectMapper().writeValueAsString(messageDto)));
    }

    @Test
    public void testCompose400NotFound() throws Exception {
        String templateId = "template_identifier";
        Map<String, String> map = new HashMap<>();
        map.put("key1", "value1");
        map.put("key2", "value2");
        when(templateService.compose(templateId, map)).thenThrow(new TemplateIdentifierNotFound(TEMPLATE_ID_NOT_FOUND));
        mvc.perform(get(TEMPLATES_BASE_URL + "/template_identifier/compose?key1=value1&key2=value2")
                .accept(APPLICATION_JSON))
                .andExpect(status().is(BAD_REQUEST.value()))
                .andExpect(content().string(TEMPLATE_ID_NOT_FOUND));
    }
}
