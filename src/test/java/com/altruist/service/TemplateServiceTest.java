package com.altruist.service;

import com.altruist.config.TemplateServiceConfig;
import com.altruist.dto.MessageDto;
import com.altruist.dto.TemplateDto;
import com.altruist.exception.TemplateIdentifierNotFound;
import com.altruist.exception.TemplateWithSameIdentifierAlreadyExists;
import com.altruist.mapper.TemplateMapper;
import com.altruist.model.Template;
import com.altruist.repository.TemplateRepository;
import com.google.common.collect.Iterables;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static com.altruist.AltruistAssignmentApp.*;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * @author ialzhanov
 * @since 2/7/2020
 */
@RunWith(SpringRunner.class)
@Import(value = {TemplateServiceConfig.class})
@TestPropertySource(properties = {"templateService.delimiter=$"})
public class TemplateServiceTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private TemplateService templateService;

    @MockBean
    private TemplateMapper templateMapper;

    @MockBean
    private TemplateRepository templateRepository;

    /**
     * Test saving a new template
     */
    @Test
    public void testSubmit() {
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("template_identifier_1");
        templateDto.setText("hello, $hello_name");
        Template temp = new Template();
        temp.setId("template_identifier_1");
        temp.setText("hello, $hello_name");
        when(templateMapper.mapFromDtoToModel(templateDto)).thenReturn(temp);
        when(templateRepository.existsById(temp.getId())).thenReturn(FALSE);
        when(templateRepository.save(temp)).thenReturn(temp);
        Template template = templateService.submit(templateDto);
        assertEquals(template.getId(), templateDto.getId());
        assertEquals(template.getText(), templateDto.getText());
    }

    /**
     * Test saving template with existing identifier
     */
    @Test
    public void testSubmitWithSameId() {
        thrown.expect(TemplateWithSameIdentifierAlreadyExists.class);
        thrown.expectMessage(containsString(TEMPLATE_ID_EXISTED));
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("template_identifier_1");
        templateDto.setText("hello, $hello_name");
        Template temp = new Template();
        temp.setId("template_identifier_1");
        temp.setText("hello, $hello_name");
        when(templateMapper.mapFromDtoToModel(templateDto)).thenReturn(temp);
        when(templateRepository.existsById(temp.getId())).thenReturn(TRUE);
        when(templateRepository.save(temp)).thenReturn(temp);
        templateService.submit(templateDto);
    }

    /**
     * Test retrieve templates when no templates are stored
     */
    @Test
    public void testRetrieveNoTemplates() {
        when(templateRepository.findAll()).thenReturn(Collections.emptyList());
        Iterable<TemplateDto> result = templateService.retrieve();
        assertFalse(result.iterator().hasNext());
    }

    /**
     * Test retrieve templates
     */
    @Test
    public void testRetrieve() {
        List<Template> list = new LinkedList<>();
        Template template = new Template();
        template.setId("template_identifier_22");
        template.setText("hello, $hello_name2222");
        list.add(template);
        TemplateDto templateDto = new TemplateDto();
        templateDto.setId("template_identifier_22");
        templateDto.setText("hello, $hello_name2222");
        when(templateMapper.mapFromModelToDto(template)).thenReturn(templateDto);
        template = new Template();
        template.setId("template_identifier_33");
        template.setText("hello, $hello_name3333");
        list.add(template);
        templateDto = new TemplateDto();
        templateDto.setId("template_identifier_33");
        templateDto.setText("hello, $hello_name3333");
        when(templateMapper.mapFromModelToDto(template)).thenReturn(templateDto);
        template = new Template();
        template.setId("template_identifier_44");
        template.setText("hello, $hello_name4444");
        list.add(template);
        templateDto = new TemplateDto();
        templateDto.setId("template_identifier_44");
        templateDto.setText("hello, $hello_name4444");
        when(templateMapper.mapFromModelToDto(template)).thenReturn(templateDto);
        when(templateRepository.findAll()).thenReturn(list);
        Iterable<TemplateDto> result = templateService.retrieve();
        assertEquals(3, Iterables.size(result));
        result.forEach(td -> {
            if ("template_identifier_22".equals(td.getId())) {
                assertEquals("hello, $hello_name2222", td.getText());
            } else if ("template_identifier_33".equals(td.getId())) {
                assertEquals("hello, $hello_name3333", td.getText());
            } else if ("template_identifier_44".equals(td.getId())) {
                assertEquals("hello, $hello_name4444", td.getText());
            }
        });
    }

    /**
     * Test message compose
     */
    @Test
    public void testCompose() {
        String templateId = "template_0";
        Template template = new Template();
        template.setId(templateId);
        template.setText("Hello, dear $title$name!");
        when(templateRepository.findById(templateId)).thenReturn(Optional.of(template));
        Map<String, String> map = new HashMap<>();
        map.put("title", "Mr.");
        map.put("name", "Wayne");
        MessageDto messageDto = templateService.compose(templateId, map);
        assertNotNull(messageDto);
        assertEquals("Hello, dear Mr.Wayne!", messageDto.getText());
    }

    /**
     * Test composing message when template with the given identifier does not exist
     */
    @Test
    public void testComposeNonExistentMessage() {
        thrown.expect(TemplateIdentifierNotFound.class);
        thrown.expectMessage(containsString(TEMPLATE_ID_NOT_FOUND));
        templateService.compose("non_existent_identifier", Collections.emptyMap());
    }

    /**
     * Test composing message when template variables were provided
     */
    @Test
    public void testComposeWithMissingTemplateVariables() {
        String templateId = "template_100";
        Template template = new Template();
        template.setId(templateId);
        template.setText("Hello, dear $title.$name!");
        when(templateRepository.findById(templateId)).thenReturn(Optional.of(template));
        Map<String, String> map = new HashMap<>();
        map.put("title", "Mr");
        MessageDto messageDto = templateService.compose(templateId, map);
        assertNotNull(messageDto);
        assertEquals("Hello, dear Mr.$name!", messageDto.getText());
    }

    /**
     * Test composing message when no template variables were provided
     */
    @Test
    public void testComposeWithoutTemplateVariables() {
        String templateId = "template_10";
        Template template = new Template();
        template.setId(templateId);
        template.setText("Hello, $title.$name!");
        when(templateRepository.findById(templateId)).thenReturn(Optional.of(template));
        MessageDto messageDto = templateService.compose(templateId, null);
        assertNotNull(messageDto);
        assertEquals("Hello, $title.$name!", messageDto.getText());
    }

    @Test
    public void testParseWithEmptyMessage() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(containsString(TEMPLATE_SERVICE_PARSE_MESSAGE_CANNOT_BE_NULL));
        templateService.parse(null, null);
    }

    @Test
    public void testParseWithoutVariables() {
        String message = "Testing $test_name";
        assertEquals(message, templateService.parse(message, null));
    }

    @Test
    public void testParseWithMissingVariables() {
        String message = "Must be between $min and $max";
        Map<String, String> variableMap = new HashMap<>();
        variableMap.put("min", "3");
        assertEquals("Must be between 3 and $max", templateService.parse(message, variableMap));
    }

    @Test
    public void testParse() {
        String message = "Hello, $title$last_name";
        Map<String, String> variableMap = new HashMap<>();
        variableMap.put("title", "Dr.");
        variableMap.put("last_name", "Doe");
        assertEquals("Hello, Dr.Doe", templateService.parse(message, variableMap));
    }

    @Test
    public void testParseWithKeysHavingDelimiter() {
        String message = "Hello, $title$last_name";
        Map<String, String> variableMap = new HashMap<>();
        variableMap.put("$title", "Mr.");
        variableMap.put("$last_name", "Doe2");
        assertEquals("Hello, Mr.Doe2", templateService.parse(message, variableMap));
    }
}
