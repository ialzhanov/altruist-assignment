package com.altruist.repository;

import com.altruist.model.Template;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityExistsException;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;

import static com.altruist.AltruistAssignmentApp.*;
import static org.hamcrest.Matchers.containsString;

/**
 * Class tests template entity validation that happens prior to persisting the template object
 *
 * @author ialzhanov
 * @since 2/7/2020
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class TemplateRepositoryTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private TestEntityManager entityManager;

    /**
     * Tests exception handling when entity identifier is not set
     */
    @Test
    public void testIdNotProvidedConstraintViolation() {
        thrown.expect(PersistenceException.class);
        thrown.expectMessage(containsString("ids for this class must be manually assigned before calling"));
        Template template = new Template();
        template.setText("sample template text");
        entityManager.persist(template);
        entityManager.flush();
    }

    /**
     * Tests exception handling when entity with the same identifier already exists
     */
    @Test
    public void testEntityExistsException() {
        thrown.expect(EntityExistsException.class);
        thrown.expectMessage(containsString(
                "A different object with the same identifier value was already associated with the session"));
        Template template1 = new Template();
        template1.setId("template_identifier_0");
        template1.setText("Template 1 text");
        entityManager.persist(template1);
        entityManager.flush();
        Template template2 = new Template();
        template2.setId("template_identifier_0");
        template2.setText("Template 2 text");
        entityManager.persist(template2);
        entityManager.flush();
    }

    /**
     * Tests exception handling when template does not have message text
     */
    @Test
    public void testTextMustNotBeNull() {
        thrown.expect(ConstraintViolationException.class);
        thrown.expectMessage(containsString(TEMPLATE_TEXT_CANNOT_BE_NULL));
        Template template = new Template();
        template.setId("template_identifier_1");
        entityManager.persist(template);
        entityManager.flush();
    }

    /**
     * Test exception handling when template text is too short
     */
    @Test
    public void testTextTooShortConstraintViolation() {
        thrown.expect(ConstraintViolationException.class);
        thrown.expectMessage(containsString(TEMPLATE_TEXT_SIZE_BETWEEN_MESSAGE));
        Template template = new Template();
        template.setId("template_identifier_2");
        template.setText(TEMPLATE_TEXT_TOO_SHORT);
        entityManager.persist(template);
        entityManager.flush();
    }

    /**
     * Test exception handling when template text is too long
     */
    @Test
    public void testTextTooLongConstraintViolation() {
        thrown.expect(ConstraintViolationException.class);
        thrown.expectMessage(containsString(TEMPLATE_TEXT_SIZE_BETWEEN_MESSAGE));
        Template template = new Template();
        template.setId("template_identifier_3");
        template.setText(TEMPLATE_TEXT_TOO_LONG);
        entityManager.persist(template);
        entityManager.flush();
    }
}