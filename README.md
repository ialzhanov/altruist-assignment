## Welcome
Test assignment for altruist

#### Introduction
Profiles enabled by default: dev, embedded, local

#### H2 enabled
To simplify data access, this project uses an embedded database H2

* Once started, H2 UI can be accessed as follows
1. URL http://localhost:8080/h2-console
2. User name: dev
3. Password:
4. JDBC URL: jdbc:h2:mem:devdb

#### Swagger enabled
This project has Swagger enabled. This means all endpoints in this project are OpenAPI documented
- HTML formatted documentation -> http://localhost:8080/swagger-ui.html#/
- Machine readable API http://localhost:8080/v2/api-docs
